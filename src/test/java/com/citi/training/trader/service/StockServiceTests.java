package com.citi.training.trader.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
public class StockServiceTests {
	@Autowired
	private StockService stockService;
	
	@MockBean
	private StockDao mockStockDao;
	
	@Test
	public void test_createRuns() {

        Stock testStock = new Stock(-1, "NFLX");
		
		int newId = 1;
		when(mockStockDao.create(any(Stock.class))).thenReturn(newId);
		int createdId = stockService.create(testStock);
		
		verify(mockStockDao).create(testStock);
		assertEquals(newId,createdId);
		
	}
	
	@Test
	public void test_deleteRuns() {
		int existingId = 1;
		//when(mockStockDao.deleteById(existingId)).thenReturn(existingId);
		//stockService.deleteById(existingId);
		
		stockService.deleteById(existingId);
		verify(mockStockDao).deleteById(existingId);
	}
	
	@Test
	public void test_findByIdRuns() {
		int testId = 66;
		
        Stock testStock = new Stock(66, "NFLX");
        
		when(mockStockDao.findById(testId)).thenReturn(testStock);
		
		Stock returnedStock = stockService.findById(testId);
		
		verify(mockStockDao).findById(testId);
		assertEquals(testStock,returnedStock);
	}
	
	@Test
	public void test_findByTicker() { 
		String testTkr = "MSFT";
        Stock testStock = new Stock(66, "MSFT");		
		
		when(mockStockDao.findByTicker(testTkr)).thenReturn(testStock);
        
		Stock returnedStock = stockService.findByTicker(testTkr);

		
		verify(mockStockDao).findByTicker(testTkr);
		assertEquals(testStock,returnedStock);
	}
	
	@Test
	public void test_findAll() { 
		List<Stock> testList = new ArrayList<Stock>();
		testList.add(new Stock(33, "NFLX"));
		
		when(mockStockDao.findAll()).thenReturn(testList);
		
		List<Stock> returnedList = stockService.findAll();
		
		//verify(mockStockDao).findAll();
		assertEquals(testList, returnedList);

	}
}
