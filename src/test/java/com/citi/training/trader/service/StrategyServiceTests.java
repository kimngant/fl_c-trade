package com.citi.training.trader.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.model.ComplexStrategy;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
public class StrategyServiceTests {
	@Autowired
	private StrategyService strategyService;
	
	@MockBean
	private StrategyDao mockStrategyDao;
	
	@Test
	public void test_createRuns() {

    	Stock stock = new Stock(1, "NFLX");
    	ComplexStrategy testComplex = new ComplexStrategy(stock, 8);
		
		int newId = 1;
		when(mockStrategyDao.save(any(ComplexStrategy.class))).thenReturn(newId);
		int createdId = strategyService.save(testComplex);
		
		verify(mockStrategyDao).save(testComplex);
		assertEquals(newId,createdId);
		
	}
	
	@Test
	public void test_findAll() { 
		List<ComplexStrategy> testList = new ArrayList<ComplexStrategy>();
		
    	Stock stock = new Stock(1, "NFLX");
    	ComplexStrategy testComplex = new ComplexStrategy(stock, 8);

		
		testList.add(testComplex);
		
		
		when(mockStrategyDao.findAll()).thenReturn(testList);
		
		List<ComplexStrategy> returnedList = strategyService.findAll();
		
		//verify(mockStockDao).findAll();
		assertEquals(testList, returnedList);

	}
	
	@Test
	public void test_findAllActive() { 
	
		List<ComplexStrategy> testList = new ArrayList<ComplexStrategy>();
		
    	Stock stock = new Stock(1, "NFLX");
    	ComplexStrategy testComplex = new ComplexStrategy(stock, 8);

		
		testList.add(testComplex);

		
		when(mockStrategyDao.findAllActive()).thenReturn(testList);
        
		List<ComplexStrategy> returnedList = strategyService.findAllActive();

		
		verify(mockStrategyDao).findAllActive();
		assertEquals(testList,returnedList);
	}
	
	@Test
	public void test_findAllInactive() { 
	
		List<ComplexStrategy> testList = new ArrayList<ComplexStrategy>();
		
    	Stock stock = new Stock(1, "NFLX");
    	ComplexStrategy testComplex = new ComplexStrategy(stock, 8);

		
		testList.add(testComplex);

		
		when(mockStrategyDao.findAllInactive()).thenReturn(testList);
        
		List<ComplexStrategy> returnedList = strategyService.findAllInactive();

		
		verify(mockStrategyDao).findAllInactive();
		assertEquals(testList,returnedList);
	}
	
	@Test
	public void test_findByIdRuns() {
		int testId = 66;
		
    	Stock stock = new Stock(1, "NFLX");
    	ComplexStrategy testStrategy = new ComplexStrategy(stock, 8);

        testStrategy.setId(testId);
        
		when(mockStrategyDao.findById(testId)).thenReturn(testStrategy);
		
		ComplexStrategy returnedStrategy = strategyService.findById(testId);
		
		verify(mockStrategyDao).findById(testId);
		assertEquals(testStrategy,returnedStrategy);
	}
}
