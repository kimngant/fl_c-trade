package com.citi.training.trader.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.dao.TradeDao;
import com.citi.training.trader.model.ComplexStrategy;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
public class TradeServiceTests {

	@Autowired
	private TradeService tradeService;
	
	@MockBean
	private TradeDao mockTradeDao;
	
	@Test
	public void test_createRuns() {
		Stock newStock = new Stock(2, "AAPL");
		double price = 20;
		int size = 10;
		
		ComplexStrategy strategy  = new ComplexStrategy(newStock, size);
		
		Trade testTrade = new Trade (newStock, price, size, 
									 Trade.TradeType.BUY, strategy);
		
		
		int newId = 1;
		when(mockTradeDao.save(any(Trade.class))).thenReturn(newId);
		int createdId = tradeService.save(testTrade);
		
		verify(mockTradeDao).save(testTrade);
		assertEquals(newId,createdId);
		
	}
	
	@Test
	public void test_deleteRuns() {
		int existingId = 1;
		
		tradeService.deleteById(existingId);
		verify(mockTradeDao).deleteById(existingId);
	}
	
	@Test
	public void test_findByStrategyRuns() {
		int testId = 66;
		
		List<Trade> testList = new ArrayList<Trade>();
		
		Stock newStock = new Stock(2, "AAPL");
		double price = 20;
		int size = 10;
		
		ComplexStrategy strategy  = new ComplexStrategy(newStock, size);
		
		strategy.setId(testId);
		
		Trade testTrade = new Trade (newStock, price, size, 
									 Trade.TradeType.BUY, strategy);
		
		testList.add(testTrade);

        
		when(mockTradeDao.findByStrategy(testId)).thenReturn(testList);
		
		List<Trade> returnedList = tradeService.findByStrategy(testId);
		
		assertEquals(testList,returnedList);
	}
	
	@Test
	public void test_findByStateRuns() {
		
		List<Trade> testList = new ArrayList<Trade>();
		
		Stock newStock = new Stock(2, "AAPL");
		double price = 20;
		int size = 10;
		
		ComplexStrategy strategy  = new ComplexStrategy(newStock, size);
		
		Trade testTrade = new Trade (newStock, price, size, 
									 Trade.TradeType.BUY, strategy);
		testTrade.setState(TradeState.FILLED);
		
		testList.add(testTrade);

        
		when(mockTradeDao.findAllByState(TradeState.FILLED)).thenReturn(testList);
		
		List<Trade> returnedList = tradeService.findAllByState(TradeState.FILLED);
		
		assertEquals(testList,returnedList);
	}
	
	@Test
	public void test_findAll() { 
		List<Trade> testList = new ArrayList<Trade>();
		
		
		Stock newStock = new Stock(2, "AAPL");
		double price = 20;
		int size = 10;
		
		ComplexStrategy strategy  = new ComplexStrategy(newStock, size);

		
		Trade testTrade = new Trade (newStock, price, size, 
									 Trade.TradeType.BUY, strategy);
		
		testList.add(testTrade);

		
		when(mockTradeDao.findAll()).thenReturn(testList);
		
		List<Trade> returnedList = tradeService.findAll();
		
		assertEquals(testList, returnedList);

	}
	
	@Test
	public void test_findByIdRuns() {
		int testId = 66;
		
		Stock newStock = new Stock(2, "AAPL");
		double price = 20;
		int size = 10;
		
		ComplexStrategy strategy  = new ComplexStrategy(newStock, size);		
		Trade testTrade = new Trade (newStock, price, size, 
									 Trade.TradeType.BUY, strategy);
        testTrade.setId(testId);
		
		when(mockTradeDao.findById(testId)).thenReturn(testTrade);
		
		Trade returnedTrade = tradeService.findById(testId);
		
		verify(mockTradeDao).findById(testId);
		assertEquals(testTrade,returnedTrade);
	}
	
	
}
