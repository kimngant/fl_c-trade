package com.citi.training.trader.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trader.dao.PriceDao;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
public class PriceServiceTests {

	@Autowired
	private PriceService priceService;
	
	@MockBean
	private PriceDao mockPriceDao;
	
	@Test
	public void test_findAllRuns() {
		
		Stock stock = new Stock(1, "TST_1");
		List<Price> testList = new ArrayList<Price>();
		testList.add(new Price(stock,98.01));
		
		when(mockPriceDao.findAll(stock)).thenReturn(testList);
		
		List<Price> returnedList = priceService.findAll("TST_1");
		
		//assertEquals(testList.size(), returnedList.size());
		
	}
	
	
}
