package com.citi.training.trader.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.TradeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@WebMvcTest(TradeController.class)
public class TradeControllerTests {
    private static final Logger logger =
            LoggerFactory.getLogger(TradeControllerTests.class);

	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private TradeService mockTradeService;
	
	@Test
	public void findAllTrades_returnsList() throws Exception {
        when(mockTradeService.findAll())
        .thenReturn(new ArrayList<Trade>());
        
        MvcResult result = this.mockMvc
                .perform(get("/api/trade" + "/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber())
                .andReturn();

        logger.info("Result from tradeDao.findAll: "
                    + result.getResponse().getContentAsString());

	}
    @Test
    public void createTrade_returnsCreated() throws Exception {
        Trade testTrade = new Trade();
        Stock stock = new Stock(1, "TST");
        
        testTrade.setId(1);
        testTrade.setStock(stock);
        

        when(mockTradeService
                .save(any(Trade.class)))
            .thenReturn(testTrade.getId());

        this.mockMvc.perform(
                post("/api/trade" + "/")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(testTrade)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id")
                    .value(testTrade.getId()))
                .andReturn();
        logger.info("Result from Create Trade");
    }

    @Test
    public void deleteTrade_returnsOK() throws Exception {
        MvcResult result = this.mockMvc
                .perform(delete("/api/trade" + "/1"))
                .andExpect(status().isNoContent())
                .andReturn();

        logger.info("Result from tradeDao.delete: "
                    + result.getResponse().getContentAsString());
    }
}
