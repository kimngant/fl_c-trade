package com.citi.training.trader.rest;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.ComplexStrategy;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeType;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2test")
@Transactional
public class TradeControllerIT {
    private static final Logger logger =
            LoggerFactory.getLogger(TradeControllerIT.class);

	@Autowired
	private TestRestTemplate restTemplate;
	
	@Value("${com.citi.trading.trader.rest.stock-base-path:/stock}")
	private String stockBasePath;
	
	@Value("${com.citi.trading.trader.rest.trade-base-path:/trade}")
	private String tradeBasePath;
	
	@Test
	public void findAll_returnsList() {
    	Stock stock = new Stock(1, "TST");
    	ComplexStrategy complex = new ComplexStrategy(stock, 8);
    	Trade trade = new Trade(stock, 3.22, 3, TradeType.SELL, complex);
    	trade.setId(2);
    	
    	
    	restTemplate.postForEntity(stockBasePath, stock, Stock.class);
	    restTemplate.postForEntity(tradeBasePath, trade, Trade.class);
	
	    ResponseEntity<List<Trade>> findAllResponse = restTemplate.exchange(
	                            tradeBasePath,
	                            HttpMethod.GET,
	                            null,
	                            new ParameterizedTypeReference<List<Trade>>(){});
	

	    assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
	    assert(findAllResponse.getBody() != null);

	    //assertThat(findAllResponse.getBody().get(0).getId(), equalTo(2));
	    //logger.debug("TRADE CONTROLLER IT TEST" + findAllResponse.getBody());
	    //assertEquals(findAllResponse.getBody().get(0).getId(),trade.getId());
	    //assertEquals(findAllResponse.getBody().get(0).getStockTicker(),trade.getStockTicker());
	}

}
