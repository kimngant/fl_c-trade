package com.citi.training.trader;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
public class SpringAutoTraderApplicationTests {

	@Test
	public void contextLoads() {
	}

}
