package com.citi.training.trader.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import javax.validation.constraints.Size;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.model.ComplexStrategy;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.Trade.TradeType;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlTradeDaoTests {

    @SpyBean
    JdbcTemplate tpl;

    @Autowired
    MysqlTradeDao mysqlTradeDao;
	
    @Test(expected=DataIntegrityViolationException.class)
    public void test_saveAndFindAll_works() {
    	Stock stock = new Stock(1, "TST");
    	ComplexStrategy complex = new ComplexStrategy(stock, 8);
    	Trade trade = new Trade(stock, 3.22, 3, TradeType.SELL, complex);
    	mysqlTradeDao.save(trade);
    	assertThat(mysqlTradeDao.findAll().size(), equalTo(1));
    }
	
    @Test(expected=DataIntegrityViolationException.class)
    public void test_saveAndFindById_works() {
    	Stock stock = new Stock(2, "TST_2");
    	ComplexStrategy complex = new ComplexStrategy(stock, 4);
    	Trade trade = new Trade(stock, 8.22, 8, TradeType.BUY, complex);

        int newId = mysqlTradeDao.save(trade);
        assertNotNull(mysqlTradeDao.findById(newId));
    
    }
    
    @Test(expected=DataIntegrityViolationException.class)
    public void test_saveAndFindAllByState_works() {
    	Stock stock1 = new Stock(3, "TST_3");
    	ComplexStrategy complex1 = new ComplexStrategy(stock1, 4);
    	Trade trade1 = new Trade(stock1, 8.22, 8, TradeType.BUY, complex1);
    	trade1.setState(TradeState.FILLED);
    	
    	Stock stock2 = new Stock(3, "TST_3");
    	ComplexStrategy complex2 = new ComplexStrategy(stock2, 4);
    	Trade trade2 = new Trade(stock2, 8.22, 8, TradeType.BUY, complex2);
    	trade2.setState(TradeState.FILLED);
    	
        mysqlTradeDao.save(trade1);
        mysqlTradeDao.save(trade2);
        
        assertEquals(mysqlTradeDao.findAllByState(TradeState.FILLED).size(),2);
    }
    
    @Test(expected=TradeNotFoundException.class)
    public void test_saveAndDeleteById_works() {
    	Stock stock = new Stock(4, "TST_4");
    	ComplexStrategy complex = new ComplexStrategy(stock, 4);
    	Trade trade = new Trade(stock, 8.22, 8, TradeType.BUY, complex);
    	trade.setId(4);
    	mysqlTradeDao.save(trade);
    	mysqlTradeDao.deleteById(4);
    	mysqlTradeDao.findById(4);
    }
}
