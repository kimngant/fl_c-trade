package com.citi.training.trader.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlPriceDaoTests {

    @SpyBean
    JdbcTemplate tpl;

    @Autowired
    MysqlPriceDao mysqlPriceDao;
	
    @Test//(expected=DataIntegrityViolationException.class)
    public void test_createAndFindAll_works() {
    	Stock stock = new Stock(1, "TST_1");
    	mysqlPriceDao.create(new Price(stock,99.99));
    	//assertThat(mysqlPriceDao.findAll(stock).size(), equalTo(1));
    }
    
    @Test(expected=DataIntegrityViolationException.class)
    public void test_createAndFindLatest_works() {
    	Stock stock = new Stock(99,"TST_2");
    	int newId = mysqlPriceDao.create(new Price(stock,22.00));
    	assertThat(mysqlPriceDao.findLatest(stock,1),equalTo(newId));
    }
    
    @Test(expected=DataIntegrityViolationException.class)
    public void test_createAndDeleteOlderThan_works() throws ParseException {

    	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	
    	String str1 = "2010-07-29 18:45:31";
    	Date testDate = format.parse(str1);
    	
    	String str2 = "2000-07-29 18:45:31";
    	Date olderDate = format.parse(str2);

    	Stock stock = new Stock(6, "TST_3");
    	Price price = new Price(stock, 9.99,olderDate);

    	mysqlPriceDao.create(price);
    	assertThat(mysqlPriceDao.deleteOlderThan(testDate), equalTo(1));

    }
    
}
