package com.citi.training.trader.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.exceptions.StockNotFoundException;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlStockDaoTests {

    @SpyBean
    JdbcTemplate tpl;

    @Autowired
    MysqlStockDao mysqlStockDao;
    
    @Test
    public void test_createAndFindAll_works() {
    	Stock stock = new Stock(1, "TST_1");
    	mysqlStockDao.create(stock);
//    	assertThat(mysqlStockDao.findAll().size(), equalTo(1));
    }
    
    @Test
    public void test_createAndFindById_works() {
    	Stock stock = new Stock(2, "TST_2");
        int newId = mysqlStockDao.create(stock);
        assertNotNull(mysqlStockDao.findById(newId));
    }
    
    @Test
    public void test_createAndFindByTicker_works() {
    	//String tkr = "TST_TKR";
    	Stock stock = new Stock(1, "TST_TKR");
    	
    	int newId = mysqlStockDao.create(stock);
    	
    	Stock newStock = mysqlStockDao.findByTicker("TST_TKR");
    	
    	assertEquals(newStock.getId(), newId);
    	assertEquals(newStock.getTicker() ,stock.getTicker());
  
    }
    
    @Test(expected=StockNotFoundException.class)
    public void test_createAndDeleteById_works() {
    	Stock stock = new Stock(4, "TST_4");
    	mysqlStockDao.create(stock);
    	mysqlStockDao.deleteById(4);
    	mysqlStockDao.findById(4);
    }
}
