package com.citi.training.trader.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trader.model.ComplexStrategy;
import com.citi.training.trader.model.Stock;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlStrategyDaoTests {

    @SpyBean
    JdbcTemplate tpl;

    @Autowired
    MysqlStrategyDao mysqlStrategyDao;
    
    @Test//(expected=DataIntegrityViolationException.class)
    public void test_saveAndFindAll_works() {
    	Stock stock = new Stock(1, "TST");
    	ComplexStrategy complex = new ComplexStrategy(stock, 8);
    	mysqlStrategyDao.save(complex);
    	assertThat(mysqlStrategyDao.findAll().size(), equalTo(1));
    }
	
}
