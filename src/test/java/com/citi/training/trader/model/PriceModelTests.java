package com.citi.training.trader.model;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

public class PriceModelTests {

	@Test
	public void test_price_consructor1() {
		
		Stock newStock = new Stock(2, "Apple");
		double price = 20;
		
		Price testPrice = new Price (newStock, price);
		assert(testPrice.getId() == -1);
		assert(testPrice.getStock().getId() == 2 );
		assert(testPrice.getPrice() == price);		
	}	
	
	@Test
	public void test_price_consructor2() {	
		int id = 1;
		Stock newStock = new Stock(2, "Apple");
		double price = 20;
		Date date = new Date();
		
		Date newDate = new Date(1122);
		
		Price testPrice = new Price (newStock, price, date);
		
		testPrice.setRecordedAt(newDate);
		
		assert(testPrice.getId() == -1);
		assert(testPrice.getStock().getId() == 2 );
		assert(testPrice.getPrice() == price);
		assert(testPrice.getRecordedAt() == newDate);
		
		
	}	
	
	@Test
	public void test_price_consructor3() {	
		int id = 1;
		Stock newStock = new Stock(2, "Apple");
		double price = 20;
		Date date = new Date(222);
		
		Price testPrice = new Price (id, newStock, price, date);
		
		assert(testPrice.getId() == id);
		assert(testPrice.getStock().getId() == 2 );
		assert(testPrice.getPrice() == price);	
		assert(testPrice.getRecordedAt() == date);
		
	}	
	
	@Test
	public void test_price_getId() {
		int id = 1;
		int newid = 2;
		Stock newStock = new Stock(2, "Apple");
		double price = 20;
		Date date = new Date();
		
		double newPrice = 10;
		
		Price testPrice = new Price (id, newStock, price, date);
		
		testPrice.setId(newid);
		testPrice.setPrice(newPrice);
		
		assert(testPrice.getId() == newid);			
		assert(testPrice.getPrice() == newPrice);
		
	}
	
	

}
