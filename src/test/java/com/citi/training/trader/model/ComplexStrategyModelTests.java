package com.citi.training.trader.model;

import java.util.Date;

import org.junit.Test;



public class ComplexStrategyModelTests {

	@Test
	public void test_Strategy_twoParameterConstructor() {
	
    	Stock stock1 = new Stock(1, "GOOGL");
    	ComplexStrategy complex1 = new ComplexStrategy(stock1, 8);
    	assert(complex1.getId() == -1);
    	assert(complex1.getStock() == stock1);
    	assert(complex1.getSize() == 8);
    	assert(complex1.getExitProfitLoss() == 20);
    	assert(complex1.getCurrentPosition() == 0);
    	assert(complex1.getLastTradePrice() == 0);
    	assert(complex1.getProfit() == 0);
    	assert(complex1.getStopped() == null);
    	
    	Stock stock2 = new Stock(2, "MSFT");
    	ComplexStrategy complex2 = new ComplexStrategy(stock2, 2);
    	assert(complex2.getId() == -1);
    	assert(complex2.getStock() == stock2);
    	assert(complex2.getSize() == 2);
    	assert(complex2.getExitProfitLoss() == 20);
    	assert(complex2.getCurrentPosition() == 0);
    	assert(complex2.getLastTradePrice() == 0);
    	assert(complex2.getProfit() == 0);
    	assert(complex2.getStopped() == null);	
	}
	
	@Test
	public void test_Strategy_fullConstructor() {
	
    	Stock stock1 = new Stock(1, "GOOGL");
    	Date date = new Date(System.currentTimeMillis());
    	ComplexStrategy complex1 = new ComplexStrategy(22, stock1, 8, 3.0, -1,98.01,3.4,date);
 
    	assert(complex1.getId() == 22);
    	assert(complex1.getStock() == stock1);
    	assert(complex1.getSize() == 8);
    	assert(complex1.getExitProfitLoss() == 3.0);
    	assert(complex1.getCurrentPosition() == -1);
    	assert(complex1.getLastTradePrice() == 98.01);
    	assert(complex1.getProfit() == 3.4);
    	assert(complex1.getStopped() == date);
    	
//    	Stock stock2 = new Stock(2, "MSFT");
//    	ComplexStrategy complex2 = new ComplexStrategy(stock2, 2);
//    	assert(complex2.getId() == -1);
//    	assert(complex2.getStock() == stock2);
//    	assert(complex2.getSize() == 2);
//    	assert(complex2.getExitProfitLoss() == 20);
//    	assert(complex2.getCurrentPosition() == 0);
//    	assert(complex2.getLastTradePrice() == 0);
//    	assert(complex2.getProfit() == 0);
//    	assert(complex2.getStopped() == null);	
	}
	
	
	
}
