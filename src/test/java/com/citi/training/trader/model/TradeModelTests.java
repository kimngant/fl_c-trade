package com.citi.training.trader.model;

import static org.junit.Assert.*;

import org.junit.Test;


public class TradeModelTests {

	// Trade(Stock stock, double price, int size,
    // TradeType tradeType, ComplexStrategy strategy) 
	
	@Test
	public void test_trade_constructor1() {
		Stock newStock = new Stock(2, "Apple");
		double price = 20;
		int size = 10;
		
		ComplexStrategy strategy  = new ComplexStrategy(newStock, size);
		
		Trade testTrade = new Trade (newStock, price, size, 
									 Trade.TradeType.BUY, strategy);
		
		assert(testTrade.getStock() == newStock);
		assertEquals(newStock, testTrade.getStock() );
		assert(testTrade.getPrice() == price);
		assertEquals(strategy, testTrade.getStrategy() );
		assertEquals(Trade.TradeType.BUY, testTrade.getTradeType());
		
		
	
	}

}
