package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.Price;
import com.citi.training.trader.service.PriceService;

/**
 * REST Controller for {@link com.citi.training.trader.model.Price} resource.
 *
 */
@RestController
@RequestMapping("${com.citi.training.trader.rest.price-base-path:/api/price}")
public class PriceController {

    private static final Logger logger =
            LoggerFactory.getLogger(PriceController.class);

    @Autowired
    private PriceService PriceService;

    @RequestMapping(value="/{ticker}",
                    method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Price> findAll(@PathVariable String ticker) {
        logger.debug("findAll(" + ticker + ")");;
        return PriceService.findAll(ticker);
    }
}
