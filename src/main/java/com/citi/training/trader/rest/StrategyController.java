package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.ComplexStrategy;
import com.citi.training.trader.service.StrategyService;

/**
 * REST Controller for {@link com.citi.training.trader.model.ComplexStrategy} resource.
 *
 */
@RestController
@RequestMapping("${com.citi.training.trader.rest.strategy-base-path:/api/strategy}")
public class StrategyController {

	private static final Logger logger =
            LoggerFactory.getLogger(StrategyController.class);
	
	@Autowired
	StrategyService StrategyService;
		
	@RequestMapping(method=RequestMethod.GET,
            		produces=MediaType.APPLICATION_JSON_VALUE)
	public List<ComplexStrategy> findAll() {
		logger.debug("findAll()");
		
		return StrategyService.findAll();		 
	}

    @RequestMapping(value="/active",
                    method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<ComplexStrategy> findAllActive() {
        logger.debug("findAllActive()");
    
        return StrategyService.findAllActive();
    }

    @RequestMapping(value="/inactive",
                    method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<ComplexStrategy> findAllInactive() {
        logger.debug("findAllInactive()");

        return StrategyService.findAllInactive();
    }

	@RequestMapping(method=RequestMethod.POST,
		            consumes=MediaType.APPLICATION_JSON_VALUE,
		            produces=MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<ComplexStrategy> save (@RequestBody ComplexStrategy complexStrategy) {
		logger.debug("create(" + complexStrategy + ")");
		
		complexStrategy.setId(StrategyService.save(complexStrategy) );
		logger.debug("saved strategy(" + complexStrategy + ")");
		
		return new ResponseEntity<ComplexStrategy>(complexStrategy, HttpStatus.CREATED);
	}

	@RequestMapping(value="/{id}",
	                method=RequestMethod.GET,
	                produces=MediaType.APPLICATION_JSON_VALUE)
	public ComplexStrategy findById(@PathVariable("id") int id) {
	    return StrategyService.findById(id);
	}
}
