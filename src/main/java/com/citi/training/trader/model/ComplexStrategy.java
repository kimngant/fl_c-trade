package com.citi.training.trader.model;

import java.util.Date;

/**
 * ComplexStrategy Model used to construct
 *  ComplexStrategy objects
 *
 */
public class ComplexStrategy {
	   private static final int DEFAULT_MAX_TRADES = 20;

	    private int id;
	    private Stock stock;
	    private int size;  	    
	    private double exitProfitLoss;
	    private int currentPosition;
	    private double lastTradePrice;
	    private double profit;
	    private Date stopped;
	    
	    public ComplexStrategy() {}
	    
	    public ComplexStrategy (Stock stock, int size) {
	    	this(-1, stock, size,DEFAULT_MAX_TRADES, 0, 0, 0, null);
	    }
	    
	    public ComplexStrategy (int id, Stock stock, int size,
	    					   double exitProfitLoss, int currentPosition,
	                           double lastTradePrice, double profit,
	                           Date stopped) {
	    	this.id = id;
	        this.stock = stock;
	        this.size = size;
	        
	        this.exitProfitLoss = exitProfitLoss;
	        this.currentPosition = currentPosition;
	        this.lastTradePrice = lastTradePrice;
	        this.profit = profit;
	        this.stopped = stopped;	    	
	    }
	    
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public Stock getStock() {
			return stock;
		}

		public void setStock(Stock stock) {
			this.stock = stock;
		}

		public int getSize() {
			return size;
		}

		public void setSize(int size) {
			this.size = size;
		}

		public double getExitProfitLoss() {
			return exitProfitLoss;
		}

		public void setExitProfitLoss(double exitProfitLoss) {
			this.exitProfitLoss = exitProfitLoss;
		}

		public int getCurrentPosition() {
			return currentPosition;
		}

		public void setCurrentPosition(int currentPosition) {
			this.currentPosition = currentPosition;
		}

		public double getLastTradePrice() {
			return lastTradePrice;
		}

		public void setLastTradePrice(double lastTradePrice) {
			this.lastTradePrice = lastTradePrice;
		}

		public double getProfit() {
			return profit;
		}

		public void setProfit(double profit) {
			this.profit = profit;
		}

		public Date getStopped() {
			return stopped;
		}

		public void setStopped(Date stopped) {
			this.stopped = stopped;
		}
		
		/**
		 * ComplexStrategy function methods
		 *
		 */
		
		public void addProfitLoss(double profitLoss) {
	        this.profit += profitLoss;
	        if(Math.abs(this.profit) >= exitProfitLoss) {
	            this.setStopped(new Date());
	        }
	    }

	    public void stop() {
	        this.stopped = new Date();
	    }

	    public boolean hasPosition() {
	        return this.currentPosition != 0;
	    }

	    public boolean hasShortPosition() {
	        return this.currentPosition < 0;
	    }

	    public boolean hasLongPosition() {
	        return this.currentPosition > 0;
	    }

	    public void takeShortPosition() {
	        this.currentPosition = -1;
	    }

	    public void takeLongPosition() {
	        this.currentPosition = 1;
	    }

	    public void closePosition() {
	        this.currentPosition = 0;
	    }   	    
}
