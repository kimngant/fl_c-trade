package com.citi.training.trader.dao;

import java.util.List;

import com.citi.training.trader.model.ComplexStrategy;

/**
 * StrategyDao Interface that will
 * interact with the complex_strategy table in database.
 *
 */
public interface StrategyDao {

    List<ComplexStrategy> findAll();
    List<ComplexStrategy> findAllActive();
    List<ComplexStrategy> findAllInactive();
    ComplexStrategy findById(int id);
    int save(ComplexStrategy strategy);
    
}
