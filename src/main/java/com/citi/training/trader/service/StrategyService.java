package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.StockDao;
import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.exceptions.StockNotFoundException;
import com.citi.training.trader.model.ComplexStrategy;
import com.citi.training.trader.model.Stock;

/**
 * Interacts with StockDAO and StrategyDAO and contains
 * additional functions for StockDAO
 *
 */
@Component
public class StrategyService {

	@Autowired
	private StrategyDao strategyDao;

	@Autowired
	private StockDao stockDao;

	public List<ComplexStrategy> findAll() {
		return strategyDao.findAll();
	}
	
	public List<ComplexStrategy> findAllActive() {
	    return strategyDao.findAllActive();
	}

    public List<ComplexStrategy> findAllInactive() {
        return strategyDao.findAllInactive();
    }

	public int save(ComplexStrategy strategy) {
	    // Insert stock if it doesn't exist
	    Stock stock = null;
	    try {
	        stock = stockDao.findByTicker(strategy.getStock().getTicker());
	    } catch(StockNotFoundException e) {}
	    if (stock == null) {
	        int stockId = stockDao.create(strategy.getStock());

	        stock = new Stock(stockId, strategy.getStock().getTicker());
	    }
	    strategy.setStock(stock);
	    return strategyDao.save(strategy);
	}

	public ComplexStrategy findById(int id) {
	    return strategyDao.findById(id);
	}
}
