package com.citi.training.trader.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.citi.training.trader.model.Trade;
import com.citi.training.trader.service.TradeService;

/**
 * Service class that will send a message to JMS queue
 * to send Trades.
 * 
 */
@Component
public class TradeSender {

    private final static Logger logger =
                    LoggerFactory.getLogger(TradeSender.class);

    @Autowired
    private TradeService tradeService;

    @Autowired
    private JmsTemplate jmstemplate;
       

    public void sendTrade(Trade tradeToSend) {
        logger.debug("Sending Trade " + tradeToSend);
        logger.debug("Trade XML:");
        logger.debug(Trade.toXml(tradeToSend));
        
        tradeToSend.stateChange(Trade.TradeState.WAITING_FOR_REPLY);
        tradeService.save(tradeToSend);

        jmstemplate.convertAndSend("OrderBroker", Trade.toXml(tradeToSend), message -> {
            message.setStringProperty("Operation", "update");
            message.setJMSCorrelationID(String.valueOf(tradeToSend.getId())); 
            return message; 
        });
    }

}
