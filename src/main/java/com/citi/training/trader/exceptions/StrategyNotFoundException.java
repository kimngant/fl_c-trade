package com.citi.training.trader.exceptions;

/**
 * An exception to be thrown by {@link com.citi.trading.dao.StrategyDao} implementations
 * when a requested strategy is not found.
 */
@SuppressWarnings("serial")
public class StrategyNotFoundException extends RuntimeException {
    public StrategyNotFoundException(String msg) {
        super(msg);
    }
}
