package com.citi.training.trader.strategy;

/**
 * Interface for Complex Strategy Algorithm
 *
 */
public interface StrategyAlgorithm {

    public void run();
    
}
