package com.citi.training.trader.strategy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.PriceDao;
import com.citi.training.trader.dao.StrategyDao;
import com.citi.training.trader.messaging.TradeReceiver;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.ComplexStrategy;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Trade;

/**
 * this class includes the implementation of the algorithm 
 * using the 2 way moving average strategy
 * 
 * @author Team C-Trade
 * @version last update 08/04/2019
 * 
 *
 */

@Component 
public class ComplexStrategyAlgorithm implements StrategyAlgorithm {
    private static final Logger logger =
            LoggerFactory.getLogger(ComplexStrategyAlgorithm.class);

	@Autowired
	private PriceDao priceDao;

	@Autowired
	private TradeSender tradeSender;
	
	@Autowired
	private TradeReceiver tradeReceiver;

	@Autowired
	private StrategyDao strategyDao;


	List<Double> profitArr = new ArrayList<Double>();	
	
	@Scheduled(fixedRateString = "${simple.strategy.refresh.rate_ms:5000}")
	public void run() {			
		
		logger.debug("going through the run loop");
		int longPeriod = 15;
		int shortPeriod = 5;
		
        //Remove prices older than 10 minutes
        Date cutOffTime = new Date(System.currentTimeMillis() - (10 * 60 * 1000));
        priceDao.deleteOlderThan(cutOffTime); 
        
		// loop thru list of all strategy
		for(ComplexStrategy strategy: strategyDao.findAll()) {
			
			double exitProfitLoss = strategy.getExitProfitLoss();
									
            if(strategy.getStopped() != null) {
            	logger.debug("strategy: " + strategy + 
            				 " with id: " + strategy.getId()+ "stopped ");
                continue;
            }
            if(!strategy.hasPosition()) {
                // we have no open position
            	logger.debug("strategy: " + strategy + " with id: " + strategy.getId() + 
            				 " has no position");
            	logger.debug("getting prices from price feed"); 
            	 
            	List<Price> longAvgArrray = priceDao.findLatest(strategy.getStock(),longPeriod);
                List<Price> shortAvgArray = priceDao.findLatest(strategy.getStock(),shortPeriod);
                
                
                if(longAvgArrray.size() < longPeriod || shortAvgArray.size() < shortPeriod ) {
                    logger.warn("Unable to execute strategy, not enough price data: " +
                                strategy);
                    continue;
                }
                
                double longAvgerage;
                double shortAverage;
                double currentPriceChange;
                
                // calculate long average and short average:
                longAvgerage = calculateAvg(longAvgArrray, longAvgArrray.size());
                logger.info("long avg for this one run is: " + longAvgerage);
                shortAverage = calculateAvg(shortAvgArray, shortAvgArray.size());
                logger.info("short avg for this one run is: " + shortAverage);
                
                             
                currentPriceChange = longAvgerage - shortAverage;
                logger.debug("Current Price change: " + currentPriceChange);

                if(currentPriceChange < 0.001 && currentPriceChange > -0.001) {
                    logger.debug("Insufficient price change, taking no action");
                    continue;
                }
                
                
                logger.debug("price changes at: " + currentPriceChange );            
                logger.debug("strategy existProfitLoss: " + exitProfitLoss );
                
                
                // if price do down: take short position => sell            
                if(currentPriceChange <  0 ) {
                    logger.info("Taking short position for strategy id: " + strategy.getId());
                    strategy.takeShortPosition();                                      
                    strategy.setLastTradePrice(makeTrade(strategy, Trade.TradeType.SELL));	// make trade --> send to broker
                } else {
                    
                	// if price going up => take long => buy
                    logger.debug("Taking long position for strategy id : " + strategy.getId());
                    strategy.takeLongPosition();                   
                    strategy.setLastTradePrice(makeTrade(strategy, Trade.TradeType.BUY)); // make trade --> send to broker
                }

            }else if(strategy.hasLongPosition()) {
                // close the position by selling           	
                logger.info("Long position for strategy: " + strategy.getId());
                logger.debug("strategy existProfitLoss: " + exitProfitLoss );
                        
                Price currentPrice ;
                double currentProfit;
                double lastTradePrice ;
                double profitMakePercentage;
                double exit_ProfitLoss ;
                
                lastTradePrice = strategy.getLastTradePrice();
                exit_ProfitLoss = strategy.getExitProfitLoss();
                
                
                // calculate profit
                currentPrice = priceDao.findLatest(strategy.getStock(), 1).get(0);               
                currentProfit = ( Math.abs(currentPrice.getPrice() - lastTradePrice )/lastTradePrice );                     
                
                logger.debug("Current Price: " + currentPrice.getPrice() + 
                			 " at time: " + currentPrice.getRecordedAt() + 
                			 " Trade Price: " + strategy.getLastTradePrice())	;	
                
         
                profitMakePercentage = currentProfit*100;
                
                
                if ( profitMakePercentage < exit_ProfitLoss ) {
                	logger.debug("not meeting up the goal. go to next run");
                	logger.debug("Long current profit % " + profitMakePercentage + "   goal: " + exit_ProfitLoss);             	
                	continue;
                }
               

                // sell long position
                logger.debug("meeting profit goal at: " + profitMakePercentage);
                double thisTradePrice = makeTrade(strategy, Trade.TradeType.SELL);  // make trade will send trade to broke to make deal                        
                logger.debug("Bought at: " + strategy.getLastTradePrice() + 
                			 ", Later Sold at: " + thisTradePrice);
                			 
                closePosition(thisTradePrice - strategy.getLastTradePrice(), strategy);

            } else if(strategy.hasShortPosition()) {
                // close the position by buying
                logger.debug("Short position for strategy: " + strategy.getId());               
                strategy.setExitProfitLoss(exitProfitLoss);
                logger.debug("strategy existProfitLoss: " + strategy.getExitProfitLoss() );               
                
                Price currentPrice;
                double currentProfit;
                double profitMakePercentage;
                
                // calculate profit
                currentPrice = priceDao.findLatest(strategy.getStock(), 1).get(0);               
                currentProfit = ( Math.abs(currentPrice.getPrice() - strategy.getLastTradePrice() )/ strategy.getLastTradePrice());                     
                profitMakePercentage = currentProfit*100;   
                
                logger.debug("Current Price: " + currentPrice.getPrice() + 
                			 " at time: " + currentPrice.getRecordedAt() + 
                			 " Sold Price: " + strategy.getLastTradePrice())	;	
                                                       
                if ( profitMakePercentage < strategy.getExitProfitLoss()) {
                	logger.info("Short current profit % " + profitMakePercentage + "  goal:" + exitProfitLoss);
                	continue;
                }
                              
                double thisTradePrice = makeTrade(strategy, Trade.TradeType.BUY);
                logger.debug("Sold at: " + strategy.getLastTradePrice() + 
                			 ", Later bought at: " + thisTradePrice );

                closePosition(strategy.getLastTradePrice() - thisTradePrice, strategy);
            }
            strategyDao.save(strategy);

		} // for loop for all strategy
	}//run loop
	
	
	/**
	 * Function to close the position and added the current profitLoss to total
	 * @param profitLoss
	 * @param strategy
	 */
	private void closePosition(double profitLoss, ComplexStrategy strategy) {
        logger.debug("Recording profit/loss of: " + profitLoss +
                     " for strategy: " + strategy);
        strategy.addProfitLoss(profitLoss);  
        strategy.closePosition();
    }

	
	/**
	 * Function to make trade buy initialize the Trade Type: Buy/Sell
	 * @param strategy
	 * @param tradeType BUY/SELL type
	 * @return
	 */
    private double makeTrade(ComplexStrategy strategy, Trade.TradeType tradeType) {
        Price currentPrice = priceDao.findLatest(strategy.getStock(), 1).get(0);
        tradeSender.sendTrade(new Trade(strategy.getStock(), currentPrice.getPrice(),
                              strategy.getSize(), tradeType,strategy));
              
        return currentPrice.getPrice();
    }
    
    /**
     * Function to calculate the short average and long average 
     * over a fix period of time
     * @param price Take the list of price object
     * @param size the size of the price object
     * @return
     */
	public double calculateAvg(List<Price> price, int size) {
		double sum =0;

		for (Price p: price) {
			sum += p.getPrice();
		}
		return sum/size;
	}
	




}
